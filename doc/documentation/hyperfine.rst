.. module:: gpaw.hyperfine
.. _hyperfine:

Isotropic and anisotropic hyperfine coupling paramters
======================================================

Use the :func:`hyperfine_parameters` function or the CLI tool::

    $ python3 -m gpaw.hyperfine <gpw-file>

.. autofunction:: hyperfine_parameters

See Peter E. Blöchl: https://doi.org/10.1103/PhysRevB.62.6158.

